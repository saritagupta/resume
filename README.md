# :sparkles: Denis Shatokhin, DevOps Engineer

<!-- delete_before_rendering -->
## [:pencil: PDF version of resume available on Releases page](https://gitlab.com/dshatohin/resume/-/releases)

<img title="That's me" src="./assets/photo.jpg" height=220>
<img title="That's CKA badge" src="./assets/cka.png" height=130>
<img title="That's Cilium service mesh badge" src="./assets/cilium-service-mesh.png" height=130>
<img title="That's Cilium cluster mesh badge" src="./assets/cilium-cluster-mesh.png" height=130>

:round_pushpin: **Helsinki, Finland**  
[:email: d_shatokhin@outlook.com](mailto:d_shatokhin@outlook.com)  
[:telephone_receiver: +358443004182](tel:+358443004182)  
[:paperclip: LinkedIn](https://www.linkedin.com/in/denis-shatokhin-63270285)

## :wave: About me

A purposeful and enthusiastic engineer who is open to everything new.
Following with interest how the field of IT and open source grows and develops.
I am ready to share my experience and knowledge in any available form.

Strongly advocate for total **IaC** and automation of everything.

Speaking :uk: English and :ru: Russian languages.

[:trophy: Certified Kubernetes Administrator](https://www.credly.com/badges/dc833ff4-a6c2-46e9-a52d-7fd3ebd198c8)  
[:scroll: Cilium Service Mesh](https://www.credly.com/badges/4c174411-b54b-4bfc-b856-52f02583e3f4)  
[:scroll: Cilium Cluster Mesh](https://www.credly.com/badges/97ef7356-a817-4824-92de-b921bca64ad1)

[:rocket: My Gitlab Project](https://gitlab.com/trac-app-k8s)

## :computer: My skills

There is a short list of my main skills:

- `Azure`
- `AWS`
- `Kubernetes`
- `Terraform`
- `Ansible`
- `Prometheus`
- `GitOps`
- `CI/CD`
- `Openstack`

<div class="page-break"></div>

## :clipboard: Employment history

### :office: DevOps Engineer, [Eficode](https://eficode.com/?hsLang=en)

#### February 2023 - present

```plain
As a DevOps engineer I'm managing infrastructure of a different clients
with the battle tested tools: Jenkins, Ansible, Terraform, Kubernetes,
Azure and AWS, containers and VMs, gitlab, github, bitbucket, ciops and gitops -
variety of tools and approaches is limitless in consulting company.
```

### :office: DevOps Engineer, [Selectel](https://selectel.ru/en)

#### August 2021 - February 2023

```plain
From the start I was assigned as the main platform engineer for our upcoming
PaaS product - Container Registry (Golang codebase).

Because we were working with Openstack cloud and developed by us services -
every terraform module should be written from scratch and documented for future usage.

Here I brought my Kubernetes skills to the new level and successfully passed CKA exam.

I also had part in supporting and developing Managed Kubernetes Service -
main product of our PaaS department.
Here I improved my skills in all areas by working in team of 2 senior
engineers.
Especially in observability - monitoring, logging and alerting.
```

### :office: System Administrator, [Selectel](https://selectel.ru/en)

#### March 2019 - August 2021

```plain
When I started working in this role our only one junior developer was writing
python code in Notepad++ and deploying new releases by uploading code
on single server by FTP.

Here I became interested in the IaC approach and learned about
Ansible, Terraform and Kubernetes.

Just in one year our team grown up to 4 developers, deploy became automated
and feature stands was creating for every merge request in Gitlab.

All infrastructure changes carried out by CI/CD after code review.
```

<div class="page-break"></div>

### :office: System Engineer, [Selectel](https://selectel.ru/en)

#### July 2014 - March 2019

```plain
At this position I was working as a remote hands of system administrators and
our clients.

We were installing all sorts of equipment in server racks, mounting optical
fiber patch-cords and performing other non-automatable tasks.

Listening never ending white noise of server rooms I truly understood the
meaning of the phrase "the cloud is just someone else's computer".
```
